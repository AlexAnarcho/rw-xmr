(dap-register-debug-template
  "Attach API debugger"
  (list :type "node"
        :request "attach"
        :port 18911
        :skipFiles ["<node_internals>/**"]
        :protocol "inspector"
        :stopOnEntry t
        ;; :localRoot  "${workspaceFolder}/node_modules/@redwoodjs/api-server/dist"
        ;; :remoteRoot "${workspaceFolder}/node_modules/@redwoodjs/api-server/dist"
        :sourceMaps t
        :restart t
        :name "Attach API debugger"))
