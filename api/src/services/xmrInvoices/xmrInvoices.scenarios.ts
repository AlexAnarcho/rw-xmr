import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.XmrInvoiceCreateArgs>({
  xmrInvoice: {
    one: {
      data: {
        subaddress: 'String',
        subaddressIndex: 7463193,
        requiredAmount: 9778913,
      },
    },
    two: {
      data: {
        subaddress: 'String',
        subaddressIndex: 1937735,
        requiredAmount: 2607819,
      },
    },
  },
})

export type StandardScenario = typeof standard
