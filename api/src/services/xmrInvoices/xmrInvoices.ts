import type { QueryResolvers, MutationResolvers, XmrInvoiceResolvers } from 'types/graphql'

import { Cache } from 'src/lib/Cache'
import { db } from 'src/lib/db'
import { logger, SYS_LOG } from 'src/lib/logger'
import { MoneroRPC } from 'src/lib/MoneroRPC'
import { checkSubaddressForBalance, createNewAddress } from 'src/lib/xmr'

import { updateSubaddressIndex, user, userByUsername } from '../users/users'

export const xmrInvoices: QueryResolvers['xmrInvoices'] = () => {
  return db.xmrInvoice.findMany()
}

export const xmrInvoice: QueryResolvers['xmrInvoice'] = async ({ id }) => {
  return db.xmrInvoice.findUnique({
    where: { id },
  })
}

export const xmrInvoicesByUser: QueryResolvers['xmrInvoicesByUser'] = ({ recipientId }) => {
  return db.xmrInvoice.findMany({
    where: { recipientId },
  })
}

export const unpaidXmrInvoicesByUser: QueryResolvers['unpaidXmrInvoicesByUser'] = ({
  recipientId,
}) => {
  return db.xmrInvoice.findMany({
    where: { recipientId, isPayed: false },
  })
}

export const createXmrInvoice: MutationResolvers['createXmrInvoice'] = ({ input }) => {
  return db.xmrInvoice.create({
    data: input,
  })
}

export const issueXmrInvoice: MutationResolvers['issueXmrInvoice'] = async ({
  username,
  input,
}) => {
  // sender has to pay
  const { priceInCent, description, senderId } = input

  const wallet = await Cache.getOrInitWallet(username)
  const { moneroSubaddressIndex, id } = await userByUsername({ username })
  const newIndex = moneroSubaddressIndex + 1
  const { newAddress, paymentUri, xmrAmount } = await createNewAddress(wallet, {
    subaddressIndex: newIndex,
    priceInCent,
    description,
  })
  await updateSubaddressIndex({ id, input: { subaddressIndex: newIndex } })

  return createXmrInvoice({
    input: {
      priceInCent,
      requiredXmrAmount: Math.floor(xmrAmount * 10 ** 12),
      recipientId: id,
      senderId,
      paymentUri,
      description,
      isPayed: false,
      subaddress: newAddress,
      subaddressIndex: newIndex,
    },
  })
}

export const updateXmrInvoice: MutationResolvers['updateXmrInvoice'] = ({ id, input }) => {
  return db.xmrInvoice.update({
    data: input,
    where: { id },
  })
}

export const deleteXmrInvoice: MutationResolvers['deleteXmrInvoice'] = ({ id }) => {
  return db.xmrInvoice.delete({
    where: { id },
  })
}

export const XmrInvoice: XmrInvoiceResolvers = {
  recipient: (_obj, { root }) => db.xmrInvoice.findUnique({ where: { id: root.id } }).recipient(),
  sender: (_obj, { root }) => db.xmrInvoice.findUnique({ where: { id: root.id } }).sender(),
}

// --- Wallet functions
export const initXmrWallet = async ({ username }) => {
  await Cache.getOrInitWallet(username)
  return true
}

export const checkXmrInvoiceBalance = async ({ invoiceId }) => {
  const invoice = await xmrInvoice({
    id: invoiceId,
  })
  if (!invoice) return

  // const { username, moneroSubaddressIndex } = await user({ id: invoice.recipientId })
  // const { isPayed, requiredXmrAmount, senderId } = invoice

  // const result = await Cache.checkUnpaidInvoices(username)

  return invoice
}
export const checkUnpaidXmrInvoices = async ({ username }) => {
  const user = await userByUsername({ username })

  const unpaidInvoices = await unpaidXmrInvoicesByUser({
    recipientId: user.id,
  })
  if (!unpaidInvoices) return false

  const wallet = await Cache.getOrInitWallet(username)
  const { result } = Promise.all(
    unpaidInvoices.map(async ({ subaddressIndex, requiredXmrAmount, id }) => {
      const balance = await checkSubaddressForBalance({
        subaddressIndex,
        wallet,
      })
      const isPayed = balance >= requiredXmrAmount
      if (isPayed) {
        const result = await updateXmrInvoice({
          id,
          input: { isPayed, payedXmrAmount: balance },
        })
        logger.info({ custom: { result } }, SYS_LOG.PAYED_INVOICE)
        return true
      }
    }),
  )

  const payedInvoices = result
  logger.info({ custom: { checkedInvoices: result } }, SYS_LOG.SYNC_COMPLETE)
  // const result = await Cache.updateInvoices(username)

  return true
}
