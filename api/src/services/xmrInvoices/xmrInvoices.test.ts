import {
  xmrInvoices,
  xmrInvoice,
  createXmrInvoice,
  updateXmrInvoice,
  deleteXmrInvoice,
} from './xmrInvoices'
import type { StandardScenario } from './xmrInvoices.scenarios'

// Generated boilerplate tests do not account for all circumstances
// and can fail without adjustments, e.g. Float and DateTime types.
//           Please refer to the RedwoodJS Testing Docs:
//       https://redwoodjs.com/docs/testing#testing-services
// https://redwoodjs.com/docs/testing#jest-expect-type-considerations

describe('xmrInvoices', () => {
  scenario('returns all xmrInvoices', async (scenario: StandardScenario) => {
    const result = await xmrInvoices()

    expect(result.length).toEqual(Object.keys(scenario.xmrInvoice).length)
  })

  scenario(
    'returns a single xmrInvoice',
    async (scenario: StandardScenario) => {
      const result = await xmrInvoice({ id: scenario.xmrInvoice.one.id })

      expect(result).toEqual(scenario.xmrInvoice.one)
    }
  )

  scenario('creates a xmrInvoice', async () => {
    const result = await createXmrInvoice({
      input: {
        subaddress: 'String',
        subaddressIndex: 9638306,
        requiredAmount: 9371218n,
      },
    })

    expect(result.subaddress).toEqual('String')
    expect(result.subaddressIndex).toEqual(9638306)
    expect(result.requiredAmount).toEqual(9371218n)
  })

  scenario('updates a xmrInvoice', async (scenario: StandardScenario) => {
    const original = await xmrInvoice({ id: scenario.xmrInvoice.one.id })
    const result = await updateXmrInvoice({
      id: original.id,
      input: { subaddress: 'String2' },
    })

    expect(result.subaddress).toEqual('String2')
  })

  scenario('deletes a xmrInvoice', async (scenario: StandardScenario) => {
    const original = await deleteXmrInvoice({ id: scenario.xmrInvoice.one.id })
    const result = await xmrInvoice({ id: original.id })

    expect(result).toEqual(null)
  })
})
