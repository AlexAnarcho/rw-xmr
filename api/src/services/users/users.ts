import type { QueryResolvers, MutationResolvers, UserResolvers } from 'types/graphql'
import { db } from 'src/lib/db'
import { logger, SYS_LOG } from 'src/lib/logger'

export const users: QueryResolvers['users'] = () => {
  return db.user.findMany()
}

export const user: QueryResolvers['user'] = ({ id }) => {
  return db.user.findUnique({
    where: { id },
  })
}

export const userByUsername: QueryResolvers['userByUsername'] = ({ username }) => {
  return db.user.findUnique({
    where: { username },
  })
}

export const createUser: MutationResolvers['createUser'] = ({ input }) => {
  return db.user.create({
    data: input,
  })
}

export const updateUser: MutationResolvers['updateUser'] = ({ id, input }) => {
  return db.user.update({
    data: input,
    where: { id },
  })
}

export const updateRestoreHeight: MutationResolvers['updateRestoreHeight'] = async ({
  username,
  input,
}) => {
  logger.debug({ custom: { input } }, SYS_LOG.UPDATED_RESTORE_HEIGHT)
  const result = await db.user.update({ data: input, where: { username } })
  return result
}

export const updateMoneroInfo: MutationResolvers['updateMoneroInfo'] = ({ id, input }) => {
  // validate(input.moneroPrimaryAddress, 'moneroPrimaryAddress')
  // TODO validate the input

  return db.user.update({
    data: input,
    where: { id },
  })
}

export const updateSubaddressIndex: MutationResolvers['updateSubaddressIndex'] = ({
  id,
  input,
}) => {
  const { subaddressIndex } = input
  return db.user.update({
    data: { moneroSubaddressIndex: subaddressIndex },
    where: { id },
  })
}

export const deleteUser: MutationResolvers['deleteUser'] = ({ id }) => {
  return db.user.delete({
    where: { id },
  })
}

export const User: UserResolvers = {
  SentInvoices: (_obj, { root }) => db.user.findUnique({ where: { id: root.id } }).SentInvoices(),
  ReceivedInvoices: (_obj, { root }) =>
    db.user.findUnique({ where: { id: root.id } }).ReceivedInvoices(),
}
