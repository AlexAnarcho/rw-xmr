import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.UserCreateArgs>({
  user: {
    one: { data: { username: 'String2202868', salt: 'String' } },
    two: { data: { username: 'String8819259', salt: 'String' } },
  },
})

export type StandardScenario = typeof standard
