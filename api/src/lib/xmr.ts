import type {
  BalancesChangedListener,
  MoneroWalletFull,
  OutputReceivedListener,
  SyncProgressListener,
} from 'monero-javascript'

import { createWalletFull, MoneroWalletListener } from 'monero-javascript'
import { logger, SYS_LOG } from './logger'

const stagenetNode = {
  networkType: 'stagenet',
  password: 'pass123',
  serverUri: 'http://localhost:38081',
  serverUsername: 'superuser',
  serverPassword: 'abctesting123',
  rejectUnauthorized: false, // e.g. local development
}

// --- Wallet stuff
export const createWallet = async (lang = 'English') => {
  const walletFull = await createWalletFull({
    // mnemonic omitted => generate random wallet
    language: lang,
    ...stagenetNode,
  })
  return walletFull.getMnemonic()
}

export const open = async (mnemonic: string) => {
  return createWalletFull({
    mnemonic,
    ...stagenetNode,
  })
}

export const initViewOnlyWallet = async ({ primaryAddress, privateViewKey, restoreHeight }) => {
  // Initialize the wallet
  const wallet = await createWalletFull({
    networkType: 'mainnet',
    password: process.env.MONERO_DEFAULT_PASSWORD,
    primaryAddress,
    privateViewKey,
    restoreHeight,
    server: process.env.MONERO_DAEMON_URL,
  })
  return wallet
}

export const generateSubaddress = async ({
  accountIndex,
  label,
  wallet,
}: {
  accountIndex: number
  label: string
  wallet: MoneroWalletFull
}) => {
  const address = await wallet.createSubaddress(accountIndex, label)
  return address
}

export const checkSubaddressForBalance = async ({
  subaddressIndex,
  wallet,
}: {
  subaddressIndex: number
  wallet: MoneroWalletFull
}) => {
  const result = await wallet.getSubaddress(0, subaddressIndex)
  const balance = Number(await result.getBalance()) // get the current balance of the subaddress
  return balance
}

export const createMoneroTransactionUri = ({
  address,
  amount,
  description,
}: {
  address: string
  amount: number
  description: string
}) => {
  return `monero:${address}?tx_amount=${amount}&tx_description=${description}`
}

// --- Listeners
export const createSyncProgressListener = (onSyncProgress: SyncProgressListener) =>
  new (class extends MoneroWalletListener {
    onSyncProgress(
      height: number,
      startHeight: number,
      endHeight: number,
      percentDone: number,
      message: string,
    ) {
      onSyncProgress(height, startHeight, endHeight, percentDone, message)
    }
  })() as MoneroWalletListener

export const createBalancesChangedListener = (onBalancesChanged: BalancesChangedListener) =>
  new (class extends MoneroWalletListener {
    onBalancesChanged(newBalance: BigInteger, newUnlockedBalance: BigInteger) {
      onBalancesChanged(newBalance, newUnlockedBalance)
    }
  })() as MoneroWalletListener

export const createOutputReceivedListener = (onOutputReceived: OutputReceivedListener) =>
  new (class extends MoneroWalletListener {
    onOutputReceived(output: any) {
      // onOutputReceived(output)
      const amount = output.getAmount()
      const txHash = output.getTx().getHash()
      const isConfirmed = output.getTx().isConfirmed()
      const isLocked = output.getTx().isLocked()
      onOutputReceived(output)
      return { amount, txHash, isConfirmed, isLocked }
    }
  })() as MoneroWalletListener

export async function createNewAddress(
  wallet: MoneroWalletFull,
  { subaddressIndex, priceInCent, description },
) {
  const xmrAmount = await _convertEurToXmr(priceInCent)
  // create a new Address + uri
  const newAddress = await wallet.getAddress(0, subaddressIndex)
  const paymentUri = _createMoneroTransactionUri({
    address: newAddress,
    amount: xmrAmount,
    description,
  })

  // save the new Invoice to the database
  return { newAddress, subaddressIndex, paymentUri, xmrAmount }
}

async function checkAddress({ primaryAddress, privateViewKey, restoreHeight, subaddressIndex }) {
  try {
    const wallet = await MoneroRPC.initViewOnlyWallet({
      primaryAddress,
      privateViewKey,
      restoreHeight: 2703806,
    })
    await wallet.sync()
    const result = await wallet.getSubaddress(0, subaddressIndex)
    const balance = await result.getBalance()
    return { balance, result, amount: Number(balance) }
  } catch (err) {
    console.error(err)
  }
}

async function _convertEurToXmr(priceInCent: number): Promise<number> {
  // allows to fetch currentPrice + convert eurAmount to XMR
  try {
    const response = await fetch('https://api.coinpaprika.com/v1/tickers/xmr-monero?quotes=EUR')
    const body = await response.json()
    const { price } = body.quotes?.EUR

    const pricePerCent = 1 / (Number(price) * 100)
    const xmrAmount = pricePerCent * priceInCent
    return xmrAmount
  } catch (error) {
    logger.error({ custom: error }, SYS_LOG.COINPAPRIKA_ERROR)
  }
}

function _createMoneroTransactionUri({
  address,
  amount,
  description,
}: {
  address: string
  amount: number
  description: string
}) {
  return `monero:${address}?tx_amount=${amount.toFixed(12)}&tx_description=${description}`
}
