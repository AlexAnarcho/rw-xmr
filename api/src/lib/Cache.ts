import { MoneroWalletFull, MoneroWalletListener } from 'monero-javascript'
import { find, has, propEq } from 'ramda'

import { updateRestoreHeight, userByUsername } from 'src/services/users/users'
import { db } from './db'
import { logger, SYS_LOG } from './logger'

import { createSyncProgressListener, initViewOnlyWallet } from './xmr'

interface ActiveWallet {
  username: string
  wallet: MoneroWalletFull
}

export class Cache {
  public static activeWallets: ActiveWallet[] = []

  public static async getOrInitWallet(username: string) {
    logger.info({ custom: { Cache: Cache.activeWallets } }, 'Cache')
    const cachedWallet = find(propEq('username', username))(Cache.activeWallets)
    const isFound = has('wallet')(cachedWallet)
    if (isFound) {
      logger.info(
        { custom: { username, activeWallets: Cache.activeWallets, got: cachedWallet } },
        SYS_LOG.CACHED_WALLET_FOUND,
      )

      return cachedWallet.wallet
    }

    // --- No cached wallet found => init the wallet => cache it
    logger.warn(
      { custom: { username, activeWallets: Cache.activeWallets } },
      SYS_LOG.CACHED_WALLET_NOT_FOUND,
    )

    // find the user
    const user = await userByUsername({ username })
    if (!user) {
      logger.error({ custom: { username } }, SYS_LOG.USER_NOT_FOUND)
      throw Error(SYS_LOG.USER_NOT_FOUND)
    }

    // instantiate the new wallet
    const wallet = await initViewOnlyWallet({
      primaryAddress: user.moneroPrimaryAddress,
      privateViewKey: user.moneroViewKey,
      restoreHeight: user.moneroRestoreHeight - 720,
    })

    wallet.sync(
      new (class extends MoneroWalletListener {
        onSyncProgress(height: number, startHeight, endHeight, percentDone, message) {
          logger.info(
            { custom: { height, startHeight, endHeight, percentDone, message } },
            SYS_LOG.SYNCING_WALLET,
          )
          if (percentDone === 1) {
            logger.info({ custom: { percentage: percentDone } }, SYS_LOG.SYNC_COMPLETE)
            updateRestoreHeight({ username, input: { moneroRestoreHeight: height } })
            // check all unpaid invoices for payment
          }
        }
      })(),
    )

    Cache.activeWallets.push({ username, wallet })
    return wallet
  }

  public static async updateInvoices(username: string) {
    const cachedWallet = Cache.getOrInitWallet(username)

    return {}

    // TODO query for unpaid invoices
    //
  }
}
