import { createLogger } from '@redwoodjs/api/logger'

/**
 * Creates a logger with RedwoodLoggerOptions
 *
 * These extend and override default LoggerOptions,
 * can define a destination like a file or other supported pino log transport stream,
 * and sets whether or not to show the logger configuration settings (defaults to false)
 *
 * @param RedwoodLoggerOptions
 *
 * RedwoodLoggerOptions have
 * @param {options} LoggerOptions - defines how to log, such as redaction and format
 * @param {string | DestinationStream} destination - defines where to log, such as a transport stream or file
 * @param {boolean} showConfig - whether to display logger configuration on initialization
 */
export enum SYS_LOG {
  // --- Wallet
  CACHED_WALLET_FOUND = 'Got a cached wallet',
  CACHED_WALLET_NOT_FOUND = 'Did not find wallet',
  SYNCING_WALLET = 'Syncing wallet',
  SYNC_COMPLETE = 'Sync complete',
  UPDATED_RESTORE_HEIGHT = 'Updated restore height',
  PAYED_INVOICE = 'Payed invoice',
  // --- User
  USER_NOT_FOUND = 'This user does not exist',
  // --- Thrid Party
  COINPAPRIKA_ERROR = 'Error with fetching from CoinPaprika',
}
export const logger = createLogger({ options: { level: 'info' } })
