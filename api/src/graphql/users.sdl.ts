export const schema = gql`
  type User {
    id: String!
    createdAt: DateTime!
    hashedPassword: String!
    username: String!
    salt: String!
    resetToken: String
    resetTokenExpiresAt: DateTime
    moneroViewKey: String
    moneroRestoreHeight: Int
    moneroPrimaryAddress: String
    moneroSubaddressIndex: Int
    SentInvoices: [XmrInvoice]!
    ReceivedInvoices: [XmrInvoice]!
  }

  type Query {
    users: [User!]! @skipAuth
    user(id: String!): User @requireAuth
    userByUsername(username: String!): User @skipAuth
  }

  input CreateUserInput {
    hashedPassword: String!
    username: String!
    salt: String!
    resetToken: String
    resetTokenExpiresAt: DateTime
    moneroViewKey: String
  }

  input UpdateUserInput {
    hashedPassword: String
    username: String
    salt: String
    resetToken: String
    resetTokenExpiresAt: DateTime
    moneroViewKey: String
  }

  input UpdateMoneroInfoInput {
    moneroViewKey: String!
    moneroPrimaryAddress: String!
    moneroRestoreHeight: Int!
  }

  input UpdateSubaddressIndexInput {
    subaddressIndex: Int!
  }

  input UpdateRestoreHeightInput {
    moneroRestoreHeight: Int!
  }

  type Mutation {
    createUser(input: CreateUserInput!): User! @requireAuth
    updateUser(id: String!, input: UpdateUserInput!): User! @requireAuth
    updateMoneroInfo(id: String!, input: UpdateMoneroInfoInput!): User! @requireAuth
    updateSubaddressIndex(id: String!, input: UpdateSubaddressIndexInput!): User! @skipAuth
    updateRestoreHeight(username: String!, input: UpdateRestoreHeightInput!): User @skipAuth
    deleteUser(id: String!): User! @requireAuth
  }
`
