export const schema = gql`
  type XmrInvoice {
    id: String!
    createdAt: DateTime!
    isPayed: Boolean!
    subaddress: String!
    subaddressIndex: Int!
    requiredXmrAmount: BigInt!
    payedXmrAmount: BigInt
    payedCentAmount: Int
    recipient: User
    recipientId: String!
    sender: User
    senderId: String
    paymentUri: String
    priceInCent: Int
    description: String
  }

  type Query {
    xmrInvoices: [XmrInvoice!]! @requireAuth
    xmrInvoice(id: String!): XmrInvoice @skipAuth
    xmrInvoicesByUser(recipientId: String!): [XmrInvoice!]! @skipAuth
    unpaidXmrInvoicesByUser(recipientId: String!): [XmrInvoice] @skipAuth
    checkXmrInvoiceBalance(invoiceId: String!): XmrInvoice @skipAuth
    checkUnpaidXmrInvoices(username: String!): Boolean @skipAuth
    initXmrWallet(username: String!): Boolean @skipAuth
  }

  input CreateXmrInvoiceInput {
    isPayed: Boolean
    subaddress: String!
    subaddressIndex: Int!
    requiredXmrAmount: BigInt
    recipientId: String!
    priceInCent: Int
    senderId: String
    paymentUri: String
    description: String
  }

  input IssueXmrInvoiceInput {
    priceInCent: Int!
    description: String!
    senderId: String!
    receiverId: String!
  }

  input UpdateXmrInvoiceInput {
    isPayed: Boolean
    subaddress: String
    subaddressIndex: Int
    requiredXmrAmount: Int
    payedXmrAmount: Int
    recipientId: String
    senderId: String
  }

  type Mutation {
    createXmrInvoice(input: CreateXmrInvoiceInput!): XmrInvoice! @requireAuth
    issueXmrInvoice(username: String!, input: IssueXmrInvoiceInput): XmrInvoice! @skipAuth
    updateXmrInvoice(id: String!, input: UpdateXmrInvoiceInput!): XmrInvoice! @requireAuth
    deleteXmrInvoice(id: String!): XmrInvoice! @requireAuth
  }
`
