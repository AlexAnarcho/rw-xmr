/*
  Warnings:

  - You are about to drop the column `receiverId` on the `XmrInvoice` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_XmrInvoice" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "isPayed" BOOLEAN NOT NULL DEFAULT false,
    "subaddress" TEXT NOT NULL,
    "subaddressIndex" INTEGER NOT NULL,
    "paymentUri" TEXT,
    "description" TEXT,
    "priceInCent" INTEGER,
    "requiredXmrAmount" INTEGER,
    "payedXmrAmount" INTEGER,
    "payedCentAmount" INTEGER,
    "recipientId" TEXT,
    "senderId" TEXT,
    CONSTRAINT "XmrInvoice_recipientId_fkey" FOREIGN KEY ("recipientId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT "XmrInvoice_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_XmrInvoice" ("createdAt", "description", "id", "isPayed", "payedCentAmount", "payedXmrAmount", "paymentUri", "priceInCent", "requiredXmrAmount", "senderId", "subaddress", "subaddressIndex") SELECT "createdAt", "description", "id", "isPayed", "payedCentAmount", "payedXmrAmount", "paymentUri", "priceInCent", "requiredXmrAmount", "senderId", "subaddress", "subaddressIndex" FROM "XmrInvoice";
DROP TABLE "XmrInvoice";
ALTER TABLE "new_XmrInvoice" RENAME TO "XmrInvoice";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
