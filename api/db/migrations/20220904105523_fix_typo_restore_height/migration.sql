/*
  Warnings:

  - You are about to drop the column `moneroRestorHeight` on the `User` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "hashedPassword" TEXT NOT NULL DEFAULT 'sensory',
    "username" TEXT NOT NULL,
    "salt" TEXT NOT NULL,
    "resetToken" TEXT,
    "resetTokenExpiresAt" DATETIME,
    "moneroViewKey" TEXT,
    "moneroPrimaryAddress" TEXT,
    "moneroRestoreHeight" INTEGER NOT NULL DEFAULT 0,
    "moneroSubaddressIndex" INTEGER NOT NULL DEFAULT 1
);
INSERT INTO "new_User" ("createdAt", "hashedPassword", "id", "moneroPrimaryAddress", "moneroSubaddressIndex", "moneroViewKey", "resetToken", "resetTokenExpiresAt", "salt", "username") SELECT "createdAt", "hashedPassword", "id", "moneroPrimaryAddress", "moneroSubaddressIndex", "moneroViewKey", "resetToken", "resetTokenExpiresAt", "salt", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
