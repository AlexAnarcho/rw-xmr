-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "hashedPassword" TEXT NOT NULL DEFAULT 'sensory',
    "username" TEXT NOT NULL,
    "salt" TEXT NOT NULL,
    "resetToken" TEXT,
    "resetTokenExpiresAt" DATETIME,
    "moneroViewKey" TEXT,
    "moneroPrimaryAddress" TEXT,
    "moneroRestorHeight" INTEGER NOT NULL DEFAULT 0,
    "moneroSubaddressIndex" INTEGER NOT NULL DEFAULT 1
);

-- CreateTable
CREATE TABLE "XmrInvoice" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "isPayed" BOOLEAN NOT NULL DEFAULT false,
    "subaddress" TEXT NOT NULL,
    "subaddressIndex" INTEGER NOT NULL,
    "paymentUri" TEXT,
    "description" TEXT,
    "priceInCent" INTEGER,
    "requiredXmrAmount" INTEGER,
    "payedXmrAmount" INTEGER,
    "payedCentAmount" INTEGER,
    "receiverId" TEXT,
    "senderId" TEXT,
    CONSTRAINT "XmrInvoice_receiverId_fkey" FOREIGN KEY ("receiverId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT "XmrInvoice_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
