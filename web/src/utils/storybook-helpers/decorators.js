import { AuthContext } from '@redwoodjs/auth/dist/AuthProvider'
export const LoggedInDecorator = (Story) => (
  <AuthContext.Provider
    value={{
      isAuthenticated: true,
      currentUser: {
        id: '123',
        username: 'Alex',
      },
    }}
  >
    <Story />
  </AuthContext.Provider>
)
