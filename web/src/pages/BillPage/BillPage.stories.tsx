import BillPage from './BillPage'

export const generated = (args) => {
  return <BillPage  {...args} />
}

export default { title: 'Pages/BillPage' }
