import { render } from '@redwoodjs/testing/web'

import BillPage from './BillPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('BillPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<BillPage />)
    }).not.toThrow()
  })
})
