import { useAuth } from '@redwoodjs/auth'
import { Link, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'
import InvoiceForm from 'src/components/InvoiceForm/InvoiceForm'

// TODO query for all other users

const BillPage = () => {
  const { currentUser } = useAuth()
  return (
    <>
      <MetaTags title="Bill" description="Bill page" />

      <h1>BillPage</h1>
      <InvoiceForm />
    </>
  )
}

export default BillPage
