import { Typography } from '@mui/material'
import { useAuth } from '@redwoodjs/auth'
import { Link, navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'
import { useEffect } from 'react'

const HomePage = () => {
  const { isAuthenticated } = useAuth()

  useEffect(() => {
    if (isAuthenticated) {
      navigate(routes.welcome())
    }
  }, [isAuthenticated])


  return (
    <>
      <MetaTags title="Home" description="Home page" />
      <Typography variant="h4" component="h1">Monero and Redwood - Create Invoices for your friends</Typography>
      <Typography variant="body1">You can get started by <Link to={routes.signup()}>signing up</Link></Typography>
    </>
  )
}

export default HomePage
