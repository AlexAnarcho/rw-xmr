import { LoggedInDecorator } from 'src/storybook-helpers/decorators'
import ProfilePage from './ProfilePage'

const Template = (args) => <ProfilePage {...args} />

export const Basic = Template.bind({})
Basic.decorators = [
  LoggedInDecorator
]


export default { title: 'Pages/ProfilePage' }
