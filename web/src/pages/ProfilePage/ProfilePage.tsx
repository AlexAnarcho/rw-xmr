import { Alert, Button, Grid, Stack, Typography } from '@mui/material'
import { useAuth } from '@redwoodjs/auth'
import { toast, Toaster } from "@redwoodjs/web/toast"
import { FieldError, Form, FormError, Label, NumberField, Submit, SubmitHandler, TextField, useForm } from '@redwoodjs/forms'
import { MetaTags, useMutation, useQuery } from '@redwoodjs/web'
import InfoChip from 'src/components/InfoChip/InfoChip'
import MoneroConfigCell from 'src/components/MoneroConfigCell/MoneroConfigCell'
import { useEffect, useState } from 'react'

interface FormValues {
  moneroViewKey: string
  moneroPrimaryAddress: string
}

const UPDATE_MONERO_INFO = gql`
  mutation UpdateMoneroInfoMutation($id: String!, $input: UpdateMoneroInfoInput!) {
    updateMoneroInfo(id: $id, input: $input) {
      id
      moneroViewKey
      moneroPrimaryAddress
      moneroRestoreHeight
    }
  }
`

export const QUERY = gql`
  query ($username: String!) {
    userByUsername(username: $username) {
        moneroPrimaryAddress
        moneroViewKey
        moneroRestoreHeight
      }
  }
`


const ProfilePage = () => {
  const { currentUser } = useAuth()
  const formMethods = useForm()
  const [updateKey, { loading, error }] = useMutation(UPDATE_MONERO_INFO, {
    onCompleted: () => {
      toast.success("Monero Info updated")
    }
  })

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    updateKey({ variables: { id: currentUser.id, input: data } })
    formMethods.reset()
  }
  const { data } = useQuery(QUERY, { variables: { username: currentUser.username } })
  const [moneroViewKey, setMoneroViewKey] = useState("");
  const [moneroPrimaryAddress, setMoneroPrimaryAddress] = useState("");
  const [moneroRestoreHeight, setMoneroRestoreHeight] = useState(0);
  useEffect(() => {
    setMoneroPrimaryAddress(data?.userByUsername?.moneroPrimaryAddress)
    setMoneroRestoreHeight(data?.userByUsername?.moneroRestoreHeight)
    setMoneroViewKey(data?.userByUsername?.moneroViewKey)
  }, [data])

  return (
    <>
      <MetaTags title="Profile" description="Profile page" />
      <Toaster />

      <Stack direction="column" my={3} spacing={2}>
        <Typography variant="h3" component="div" sx={{ flexGrow: 1 }}>My Monero Settings</Typography>
        <Stack spacing={2}>
          <InfoChip bodyText={currentUser.id} label="UserID" plsTruncate={true} />
          <InfoChip bodyText={currentUser.username} label="Username" />
        </Stack>
      </Stack>

      <Form onSubmit={onSubmit} formMethods={formMethods} error={error} config={{ mode: "onBlur" }}>
        <Grid container direction="row">
          <Grid item>
            <FormError error={error} wrapperClassName="form-error" />
            <Grid container direction="column" spacing={1}>
              <Grid item xs={4}>
                <InfoChip bodyText={moneroViewKey || ""} label="Private View Key" plsTruncate={true} />

              </Grid>
              <Grid item xs={8}>
                <TextField name="moneroViewKey" validation={{
                  required: true, pattern: {
                    value: /[a-f0-9]{64}/, message: "Please enter a valid view key"
                  }
                }} errorClassName="error" />
              </Grid>
              <Grid item xs={4} mt={2}>
                <InfoChip bodyText={moneroPrimaryAddress || ""} label="Primary Address" plsTruncate={true} />
              </Grid>
              <Grid item xs={8}>
                <TextField style={{ flex: "grow" }} name="moneroPrimaryAddress" validation={{
                  required: true, pattern: {
                    value: /^4[a-zA-Z0-9]{94}/, message: "Please enter a valid monero address"
                  }
                }} errorClassName="error" />
              </Grid>
              <Grid item xs={4} mt={2}>
                <InfoChip bodyText={String(moneroRestoreHeight) || ""} label="Restore Height" />
              </Grid>
              <Grid item xs={8}>
                <NumberField style={{ flex: "grow" }} name="moneroRestoreHeight" />
              </Grid>

            </Grid>
          </Grid>

          <Grid item xs={12} mt={3} >
            <Button type="submit" disabled={loading} variant="contained">Save</Button>
          </Grid>


        </Grid>

      </Form>
    </>
  )
}

export default ProfilePage
