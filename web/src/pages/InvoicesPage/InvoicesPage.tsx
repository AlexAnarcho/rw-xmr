import { Button, CircularProgress, Stack, Typography } from '@mui/material'
import { useLazyQuery } from '@apollo/react-hooks'
import XmrInvoicesCell from "src/components/XmrInvoicesCell"
import { useAuth } from '@redwoodjs/auth'
import { MetaTags } from '@redwoodjs/web'

export const QUERY = gql`
    query checkInvoices($username: String!) {checkUnpaidXmrInvoices(username: $username)}
`

const InvoicesPage = () => {
  const { currentUser } = useAuth()
  const [refreshInvoices, { loading, error, data }] = useLazyQuery(QUERY, { variables: { username: currentUser.username } })
  return (
    <>
      <MetaTags title="XMR Invoices" description="My XMR Invoices" />

      <Stack justifyContent="space-around" display="flex" direction="row" sx={{ flexGrow: 1 }}>
        <Typography variant="h3" component="h1">My XMR Invoices</Typography>
        <Button variant="contained" disabled={loading} onClick={refreshInvoices}>Check Invoices</Button>

        {loading && <CircularProgress />}
      </Stack>

      <XmrInvoicesCell recipientId={currentUser.id} />
    </>
  )
}

export default InvoicesPage
