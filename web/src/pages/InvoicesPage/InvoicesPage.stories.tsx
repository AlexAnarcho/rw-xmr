import { LoggedInDecorator } from "src/storybook-helpers/decorators"
import InvoicesPage from './InvoicesPage'


const Template = (args) => <InvoicesPage {...args} />

export const Basic = Template.bind({})
Basic.decorators = [
  LoggedInDecorator
]


export default { title: 'Pages/InvoicesPage' }
