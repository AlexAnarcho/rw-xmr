import { Typography } from '@mui/material'
import { Link, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'

const WelcomePage = () => {
  return (
    <>
      <MetaTags title="Welcome" description="Welcome page" />

      <Typography variant="h3">Sup</Typography>
    </>
  )
}

export default WelcomePage
