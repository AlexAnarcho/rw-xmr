import WelcomePage from './WelcomePage'

export const generated = (args) => {
  return <WelcomePage  {...args} />
}

export default { title: 'Pages/WelcomePage' }
