// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Router, Route, Set, Private } from '@redwoodjs/router'

import BasicLayout from 'src/layouts/BasicLayout/BasicLayout'

const Routes = () => {
  return (
    <Router>
      <Set wrap={BasicLayout}>
        <Route path="/login" page={LoginPage} name="login" />
        <Route path="/signup" page={SignupPage} name="signup" />
        <Route path="/forgot-password" page={ForgotPasswordPage} name="forgotPassword" />
        <Route path="/reset-password" page={ResetPasswordPage} name="resetPassword" />
        {/* Public routes */}
        <Route path="/" page={HomePage} name="home" />
        <Route notfound page={NotFoundPage} />
        {/* Functionality */}
        {/* Private routes */}
        <Private unauthenticated="home">
          <Route path="/welcome" page={WelcomePage} name="welcome" />
          <Route path="/profile" page={ProfilePage} name="profile" />
          <Route path="/invoices" page={InvoicesPage} name="invoices" />
          <Route path="/bill" page={BillPage} name="bill" />
        </Private>
      </Set>
    </Router>
  )
}

export default Routes
