import { atom } from 'jotai'
import { MoneroWalletFull } from 'monero-javascript'

// --- ATOMS
export const mnemonicAtom = atom('')
export const progressAtom = atom(0)
export const balanceAtom = atom(0)
export const lockedBalanceAtom = atom(0)
export const syncHeightAtom = atom(0)
export const syncEndHeightAtom = atom(0)
export const syncStartHeightAtom = atom(1102410)
export const isSyncRunningAtom = atom(false)
export const userNameAtom = atom('')
export const displayNameAtom = atom('')
export const walletAtom = atom<MoneroWalletFull | undefined>(undefined)
export const seedLangAtom = atom('English')
