import { Container, CssBaseline, Grid } from '@mui/material'
import MenuBar from 'src/components/MenuBar/MenuBar'

type BasicLayoutProps = {
  children?: React.ReactNode
}

const BasicLayout = ({ children }: BasicLayoutProps) => {

  return (
    <>
      <MenuBar />
      <Container
        sx={{
          display: 'flex',
          flexDirection: 'column',
          background: 'linear-gradient(90deg, #e3ffe7 0%, #d9e7ff 100%)',
          minHeight: '100vh',
        }}
        component="main"
        maxWidth={false}
      >
        <Grid
          container
          sx={{
            backgroundColor: '#transparent',
            marginTop: 2,
            marginBottom: 5,
            mx: 12
          }}
          component="section"
          maxWidth="sm"
          rowSpacing={3}
        >
          <CssBaseline />
          {children}
        </Grid>
      </Container>
    </>
  )
}

export default BasicLayout
