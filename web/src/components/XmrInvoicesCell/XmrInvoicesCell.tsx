import { Stack, Typography } from '@mui/material'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import XmrInvoice from '../XmrInvoice/XmrInvoice'


export const QUERY = gql`
  query xmrInvoicesByUser($recipientId: String!) {
    xmrInvoicesByUser(recipientId: $recipientId) {
      id
      createdAt
      isPayed
      subaddress
      subaddressIndex
      requiredXmrAmount
      paymentUri
      priceInCent
      description
     }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <><Typography variant="h6" color="info">There is nothing here...</Typography></>

export const Failure = ({ error }: CellFailureProps) => (
  <div style={{ color: 'red' }}>Error: {error.message}</div>
)

export const Success = ({ xmrInvoicesByUser, recipientId }: CellSuccessProps) => {
  const payedInvoices = xmrInvoicesByUser?.filter(({ isPayed }) => isPayed)
  const unpayedInvoices = xmrInvoicesByUser?.filter(({ isPayed }) => !isPayed)

  return (
    <>
      {/* Paid Invoices */}
      {payedInvoices.length > 0 &&
        <>
          <Typography variant="h4" component="h2">Payed Invoices</Typography>
          <Stack direction="column">
            {xmrInvoicesByUser?.filter(({ isPayed }) => isPayed).map((invoice) => {
              return <XmrInvoice invoice={invoice} />
            })}
          </Stack>
        </>

      }
      {unpayedInvoices.length > 0 &&
        <>
          <Typography variant="h4" component="h2">Unpayed Invoices</Typography>
          <Stack direction="column">
            {xmrInvoicesByUser?.filter(({ isPayed }) => !isPayed).map((invoice) => {
              return <XmrInvoice invoice={invoice} />
            })}
          </Stack>
        </>
      }
    </>
  )
}
