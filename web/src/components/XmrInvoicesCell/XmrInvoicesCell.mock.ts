// Define your own mock data here:
export const standard = (/* vars, { ctx, req } */) => ({
  xmrInvoicesByUser: [
    // Define your own mock data here:
    {
      id: '1e3ea37e-5b87-4f62-aebb-85563878370c',
      createdAt: '2022-08-05T13:30:37.078Z',
      isPayed: false,
      subaddress:
        '844irLkeC926KpuEPYPkYagRdkf4x5BaiDdiTHp6JiR8Mns472iLRTTHCXFruixZABjJFd2fDaaYNRSVpNVu9kFo3HfqHPN',
      paymentUri:
        'monero:86j2hBhcuJNStA8nRzmjiv9dppoJEAVoG4YujZuASHGyBMphgZ2r256iHLwB97sogWaJVvkQJyHpWbTz4qmL7DJwE17rHS7?tx_amount=NaN&tx_description=For testing',
      subaddressIndex: 1,
      priceInCent: 123,
      description: 'Payed invoice',
      requiredXmrAmount: 1,
      recipientId: '7525a0d2-f8f4-41aa-835e-8ca9c5669458',
      senderId: '2ec1076b-b1de-407d-b868-b0408fd2777f',
    },
    {
      id: '2e3ea37e-5b87-4f62-aebb-85563878370c',
      createdAt: '2022-08-06T13:30:37.078Z',
      isPayed: true,
      subaddress:
        '844irLkeC926KpuEPYPkYagRdkf4x5BaiDdiTHp6JiR8Mns472iLRTTHCXFruixZABjJFd2fDaaYNRSVpNVu9kFo3HfqHPN',
      paymentUri:
        'monero:86j2hBhcuJNStA8nRzmjiv9dppoJEAVoG4YujZuASHGyBMphgZ2r256iHLwB97sogWaJVvkQJyHpWbTz4qmL7DJwE17rHS7?tx_amount=NaN&tx_description=For testing',
      subaddressIndex: 1,
      priceInCent: 123,
      description: 'Payed invoice',
      requiredXmrAmount: 12,
      payedXmrAmount: 13,
      payedCentAmount: 123,
      recipientId: '7525a0d2-f8f4-41aa-835e-8ca9c5669458',
      senderId: '2ec1076b-b1de-407d-b868-b0408fd2777f',
    },
  ],
})
