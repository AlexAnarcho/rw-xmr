import { render } from '@redwoodjs/testing/web'

import InitWalletButton from './InitWalletButton'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('InitWalletButton', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<InitWalletButton />)
    }).not.toThrow()
  })
})
