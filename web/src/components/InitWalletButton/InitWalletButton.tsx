import { useLazyQuery } from "@apollo/react-hooks"
import { Button, CircularProgress, Typography } from "@mui/material"
import { useAuth } from "@redwoodjs/auth"
import { useAtom } from "jotai"
import { useEffect } from "react"
import { isSyncRunningAtom } from "src/store"

const QUERY = gql`
query InitWallet ($username: String!){
  initXmrWallet(username: $username)
}
`

const InitWalletButton = () => {
  const { currentUser } = useAuth()
  const [isSyncing, setIsSyncing] = useAtom(isSyncRunningAtom)

  const [initWallet, { loading, error, data }] = useLazyQuery(QUERY, { variables: { username: currentUser.username } })



  return (
    <>
      <Button disabled={loading} variant="outlined" color="inherit" onClick={initWallet}>
        {loading ? <CircularProgress /> : error ? "That didnt work" : data ? "Success" :
          <Typography variant="caption" >
            Initialize Wallet
          </Typography>

        }
      </Button>
    </>
  )
}

export default InitWalletButton
