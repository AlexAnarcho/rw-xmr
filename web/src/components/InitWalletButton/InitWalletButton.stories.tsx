import InitWalletButton from './InitWalletButton'

export const generated = (args) => {
  return <InitWalletButton {...args} />
}

export default { title: 'Components/InitWalletButton' }
