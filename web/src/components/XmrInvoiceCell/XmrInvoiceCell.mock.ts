// Define your own mock data here:
export const standard = (/* vars, { ctx, req } */) => ({
  xmrInvoice: {
    id: '1e3ea37e-5b87-4f62-aebb-85563878370c',
    createdAt: '2022-08-05T13:30:37.078Z',
    isPayed: false,
    subaddress:
      '844irLkeC926KpuEPYPkYagRdkf4x5BaiDdiTHp6JiR8Mns472iLRTTHCXFruixZABjJFd2fDaaYNRSVpNVu9kFo3HfqHPN',
    paymentUri:
      'monero:8AUYdq7XGT1cKuHoWA7M7WKJGhdUXX2mWfuVPkRHQsB5iR457oSCVnPfRusXN7iAXJJ9yZ8sC1QnAJjNCcqn47UmNM92KyR?tx_amount=0.9260043126548014&tx_description=For testing',
    subaddressIndex: 1,
    priceInCent: 123,
    description: 'Payed invoice',
    requiredXmrAmount: 1,
    recipientId: '7525a0d2-f8f4-41aa-835e-8ca9c5669458',
    senderId: '2ec1076b-b1de-407d-b868-b0408fd2777f',
  },
})
