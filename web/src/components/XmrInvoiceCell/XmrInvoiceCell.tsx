import type { FindXmrInvoiceQuery, FindXmrInvoiceQueryVariables } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import XmrInvoice from 'src/components/XmrInvoice/XmrInvoice'

export const QUERY = gql`
  query FindXmrInvoiceQuery($id: String!) {
    xmrInvoice: xmrInvoice(id: $id) {
      id
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Empty</div>

export const Failure = ({
  error,
}: CellFailureProps<FindXmrInvoiceQueryVariables>) => (
  <div style={{ color: 'red' }}>Error: {error.message}</div>
)

export const Success = ({
  xmrInvoice,
}: CellSuccessProps<FindXmrInvoiceQuery, FindXmrInvoiceQueryVariables>) => {
  return <XmrInvoice invoice={xmrInvoice} />
}
