import { Box, AppBar, Button, Toolbar, Typography, Stack } from "@mui/material"
import { useAuth } from "@redwoodjs/auth"
import { Link, routes } from "@redwoodjs/router"
import InitWalletButton from "../InitWalletButton/InitWalletButton"

const MenuBar = () => {
  const { isAuthenticated, currentUser, logOut } = useAuth()
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          {isAuthenticated ?
            <>
              <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1 }}>
                <Link to={routes.profile()} style={{ color: "white", textDecoration: "none" }}>
                  Welcome, {currentUser.username}
                </Link>
              </Typography>
              <Stack direction="row" alignItems="center" spacing={2}>
                <InitWalletButton />
                <Link to={routes.profile()} style={{ color: "white", textDecoration: "none", marginInline: 15 }}>Profile</Link>
                <Link to={routes.invoices()} style={{ color: "white", textDecoration: "none", marginInline: 15 }}>Invoices</Link>
                <Link to={routes.bill()} style={{ color: "white", textDecoration: "none", marginInline: 15 }}>Bill</Link>
              </Stack>
              <Button variant="outlined" onClick={logOut} color="inherit">Log Out</Button>
            </>
            : <>
              <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1 }}>
                Redwood meets Monero
              </Typography>
              <Button variant="outlined" color="inherit">
                <Link to={routes.login()} style={{ color: "white", textDecoration: "none" }}>Login</Link>
              </Button>
            </>
          }
        </Toolbar>
      </AppBar>
    </Box >
  )
}

export default MenuBar
