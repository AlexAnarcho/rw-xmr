import { render } from '@redwoodjs/testing/web'

import MenuBar from './MenuBar'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('MenuBar', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<MenuBar />)
    }).not.toThrow()
  })
})
