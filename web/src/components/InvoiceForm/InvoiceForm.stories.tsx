import InvoiceForm from './InvoiceForm'

export const generated = (args) => {
  return <InvoiceForm {...args} />
}

export default { title: 'Components/InvoiceForm' }
