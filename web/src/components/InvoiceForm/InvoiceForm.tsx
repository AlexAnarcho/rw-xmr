import { Button, Grid } from "@mui/material"
import { useAuth } from "@redwoodjs/auth"
import { Form, FormError, TextField, useForm, SubmitHandler, Label, NumberField } from "@redwoodjs/forms"
import { useMutation } from "@redwoodjs/web"
import { toast } from "@redwoodjs/web/dist/toast"
import UsersCell from "src/components/UsersCell"

interface FormValues {
  moneroViewKey: string
  moneroPrimaryAddress: string
}

const ISSUE_INVOICE = gql`
  mutation issueXmrInvoice($username: String!, $input: IssueXmrInvoiceInput!) {
    issueXmrInvoice(username: $username, input: $input) {
      createdAt
      description
      id
      isPayed
      payedCentAmount
      payedXmrAmount
      paymentUri
      priceInCent
      requiredXmrAmount
      recipientId
      senderId
      subaddress
      subaddressIndex
    }
  }
`

const InvoiceForm = () => {
  const { currentUser } = useAuth()
  const formMethods = useForm()
  const [issueXmrInvoice, { loading, error }] = useMutation(ISSUE_INVOICE, {
    onCompleted: () => {
      toast.success("Monero Info updated")
    }
  })
  const [user, setUser] = React.useState();
  const onSubmit: SubmitHandler<FormValues> = (data) => {
    const input = { ...data, receiverId: currentUser.id, senderId: user }
    issueXmrInvoice({ variables: { username: currentUser.username, input } })
    formMethods.reset()
  }



  return (
    <>
      <Form onSubmit={onSubmit} formMethods={formMethods} error={error} config={{ mode: "onBlur" }}>
        <Grid container direction="row">
          <Grid item>
            <FormError error={error} wrapperClassName="form-error" />

            {/* Pick the paying user */}
            <Grid container direction="column" spacing={1}>
              <Grid item xs={4}>
                {/* {moneroViewKey && <InfoChip bodyText={moneroViewKey} label="Private View Key" plsTruncate={true} />} */}
                <UsersCell getter={user} setter={setUser} />
              </Grid>
              {/* <Grid item xs={8}>
                <TextField name="sender" errorClassName="error" />
              </Grid> */}

              {/* Select the amount */}
              <Grid item xs={4} mt={2}>
                {/* {moneroPrimaryAddress && <InfoChip bodyText={moneroPrimaryAddress} label="Primary Address" plsTruncate={true} />} */}
                <Label name="priceInCent">Invoice Amount (in EUR Cent)</Label>
              </Grid>
              <Grid item xs={8}>
                <NumberField name="priceInCent" errorClassName="error" />
              </Grid>
              <Grid item xs={4} mt={2}>
                <Label name="description">Invoice Reason</Label>
              </Grid>
              <Grid item xs={8}>
                <TextField name="description" errorClassName="error" />
              </Grid>

            </Grid>
          </Grid>

          <Grid item xs={12} mt={3} >
            <Button type="submit" disabled={loading} variant="contained">Save</Button>
          </Grid>


        </Grid>

      </Form>
    </>
  )
}

export default InvoiceForm
