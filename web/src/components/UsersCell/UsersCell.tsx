import type { UsersQuery } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'
import { Box, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import { useAuth } from '@redwoodjs/auth'


export const QUERY = gql`
  query UsersQuery {
    users {
      id
      username
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Empty</div>

export const Failure = ({ error }: CellFailureProps) => (
  <div style={{ color: 'red' }}>Error: {error.message}</div>
)

export const Success = ({ users, getter, setter }) => {
  const { currentUser } = useAuth()
  const handleChange = (event: SelectChangeEvent) => {
    setter(event.target.value as string);
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Age</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={getter}
          label="Age"
          onChange={handleChange}
        >
          {users.filter(({ username }) => currentUser.username !== username).map((item) => {
            return <MenuItem value={item.id} key={item.id}>{item?.username}</MenuItem>
          })}
        </Select>
      </FormControl>
    </Box>

  )
}
