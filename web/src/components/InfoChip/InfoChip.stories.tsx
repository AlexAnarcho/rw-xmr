import InfoChip from './InfoChip'

export const generated = () => {
  return <InfoChip bodyText="3ee4cea084f01bc959eb34e0772e4e9d22cc81b743803580410786c295460c0b" label="View Key" />
}

export default { title: 'Components/InfoChip' }
