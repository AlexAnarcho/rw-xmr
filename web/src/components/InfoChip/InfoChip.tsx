import { Paper, Stack, Tooltip, Typography } from "@mui/material"
import { useEffect, useState } from "react"

interface Props {
  bodyText: string
  label: string
  plsTruncate?: boolean
}
const InfoChip = ({ bodyText, label, plsTruncate = false }: Props) => {

  const [displayText, setDisplayText] = useState("");

  useEffect(() => {
    const txt = plsTruncate ? `${bodyText.substring(0, 4)}...${bodyText.substring(bodyText.length - 4, bodyText.length + 1)}` : bodyText
    setDisplayText(txt)
  }, [bodyText])

  return (<Tooltip title={bodyText}>
    <Paper elevation={2} sx={{ display: "inline-block", py: 1, px: 2 }}>
      <Stack direction="row" spacing={4} sx={{ alignItems: "center" }}>
        <Typography variant="caption">{label}</Typography>
        <Typography variant="body1">{displayText}</Typography>
      </Stack>
    </Paper>
  </Tooltip>)
}

export default InfoChip
