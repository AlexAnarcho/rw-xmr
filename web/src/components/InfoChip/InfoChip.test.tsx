import { render } from '@redwoodjs/testing/web'

import InfoChip from './InfoChip'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('InfoChip', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<InfoChip />)
    }).not.toThrow()
  })
})
