import { useState } from 'react'

import ReceiptIcon from '@mui/icons-material/Receipt'
import { Chip, Snackbar } from '@mui/material'

const Subaddress = ({ subaddress }) => {
  const [open, setOpen] = useState(false)

  const handleClick = () => {
    setOpen(true)
    navigator.clipboard.writeText(String(subaddress))
  }

  return (
    <>
      <Chip
        icon={<ReceiptIcon color="primary" />}
        label={subaddress}
        onClick={handleClick}
      />
      <Snackbar
        open={open}
        onClose={() => setOpen(false)}
        autoHideDuration={3000}
        message="Copied XMR-address to clipboard"
      />
    </>
  )
}

export default Subaddress
