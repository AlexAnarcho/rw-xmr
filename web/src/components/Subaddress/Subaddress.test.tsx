import { render } from '@redwoodjs/testing/web'

import Subaddress from './Subaddress'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('Subaddress', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<Subaddress />)
    }).not.toThrow()
  })
})
