import { render } from '@redwoodjs/testing/web'

import XmrInvoice from './XmrInvoice'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('XmrInvoice', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<XmrInvoice />)
    }).not.toThrow()
  })
})
