import XmrInvoice from './XmrInvoice'
import { standard } from "src/components/XmrInvoicesCell/XmrInvoicesCell.mock"

export const unpayed = () => {
  return <XmrInvoice invoice={standard().xmrInvoicesByUser[0]} />
}

export const payed = () => {
  return <XmrInvoice invoice={standard().xmrInvoicesByUser[1]} />
}


export default { title: 'Components/XmrInvoice' }
