import { Box, Button, Paper, Stack, Typography } from "@mui/material"
import QRCode from "react-qr-code"
import Subaddress from "../Subaddress/Subaddress"

const XmrInvoice = ({ invoice }) => {
  const { isPayed, subaddressIndex, requiredXmrAmount, payedXmrAmount, priceInCent, payedCentAmount, recipientId, senderId, subaddress, paymentUri } = invoice

  if (isPayed === true) return (<Paper elevation={2} sx={{ m: 2, p: 3, backgroundColor: "#27a444" }}>
    <Stack>
      <Typography variant="h4" component="h2">Invoice #{subaddressIndex}</Typography>
      <Typography variant="subtitle1">Payed {payedXmrAmount} XMR ({payedCentAmount / 100} EUR)</Typography>
      <Stack direction="row" spacing={2}>
        <Typography variant="caption">From</Typography>
        <Typography variant="button">{senderId}</Typography>
        <Typography variant="caption">To</Typography>
        <Typography variant="button">{recipientId}</Typography>
      </Stack>
    </Stack>
  </Paper>
  )

  // --- Unpayed
  return (
    <Paper elevation={2} sx={{ m: 2, p: 3 }}>
      <Stack>
        <Stack>
          <Typography variant="h4" component="h2">Invoice #{subaddressIndex}</Typography>
          <Typography variant="subtitle1">Expecting {String(Number(requiredXmrAmount) / 10 ** 12)} XMR ({priceInCent / 100} EUR)</Typography>
        </Stack>
        <Stack direction="row" spacing={2}>
          <Typography variant="caption">From</Typography>
          <Typography variant="button">{senderId}</Typography>
          <Typography variant="caption">To</Typography>
          <Typography variant="button">{recipientId}</Typography>
        </Stack>
        <Box justifyContent="center" sx={{ display: "flex", flexGrow: 1, my: 2 }}>
          <QRCode value={paymentUri} />
        </Box>
        <Subaddress subaddress={subaddress} />
      </Stack>
    </Paper>
  )
}

export default XmrInvoice
